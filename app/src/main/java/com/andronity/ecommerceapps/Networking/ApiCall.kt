package com.andronity.ecommerceapps.Networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiCall{
    fun connect(): Retrofit{
        return Retrofit.Builder()
                .baseUrl("http://c29de169.ngrok.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }
}