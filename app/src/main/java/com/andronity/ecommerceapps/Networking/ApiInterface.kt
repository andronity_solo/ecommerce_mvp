package com.andronity.ecommerceapps.Networking

import com.andronity.ecommerceapps.Data.ResponseRegister
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiInterface {
    @FormUrlEncoded
    @POST( "api/customer")
    fun register(@Field ("nama") nama : String,
            @Field("username") username : String,
            @Field("password") password : String,
            @Field("jenis_kelamin") jenis_kelamin : String) : Call<ResponseRegister>
}