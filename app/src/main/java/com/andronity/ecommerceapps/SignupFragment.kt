package com.andronity.ecommerceapps

import android.content.ContentValues.TAG
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.andronity.ecommerceapps.Data.ResponseRegister
import com.andronity.ecommerceapps.Networking.ApiCall
import com.andronity.ecommerceapps.Networking.ApiInterface
import kotlinx.android.synthetic.main.signin_layout.view.*
import kotlinx.android.synthetic.main.signup_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupFragment : Fragment(){

    lateinit var views : View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        views = inflater.inflate(R.layout.signup_layout,container,false)
        setAction()
        return views
    }

    fun setAction(){
        views.btnDaftar.setOnClickListener {
            val nama = views.edit_nama_lengkap.text.toString()
            val user = views.edit_id_pengguna.text.toString()
            val pass = views.edit_sandi.text.toString()
            val jk = views.edit_jenis_kelamin.text.toString()
            val apiInterface : ApiInterface = ApiCall().connect().create(ApiInterface::class.java)
            val call : Call<ResponseRegister> = apiInterface.register(nama,user,pass,jk)
            call.enqueue(object : Callback<ResponseRegister>{
                override fun onFailure(call: Call<ResponseRegister>?, t: Throwable?) {
                    Log.i(TAG, "gagal: "+t!!.message);
                    Toast.makeText(activity,"Daftar gagal"+t!!.message,Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ResponseRegister>?, response: Response<ResponseRegister>?) {
                    Toast.makeText(activity,"Daftar berhasil : "+response,Toast.LENGTH_SHORT).show()
                }

            })
        }
    }
}